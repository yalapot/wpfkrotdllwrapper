﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Controls;
using System.Collections.ObjectModel;
using System.Windows.Input;

//using System.Windows.Documents;
//using System.Windows.Media;
//using System.Windows.Media.Imaging;
//using System.Windows.Navigation;
//using System.Windows.Shapes;

//using System.Windows.Media.Animation;

using System.Windows.Threading; // DispatcherTimer

using Microsoft.Win32; // OpenFileDialog


namespace WpfKrotDllWrapper.ViewModel
{
    public class RawDataViewModel : ViewModelBase
    {

        private ObservableCollection<SingleRawDataViewModel> viewRawBoxes = new ObservableCollection<SingleRawDataViewModel>();

        public ObservableCollection<SingleRawDataViewModel> ViewRawBoxes
        {
            get {
                return viewRawBoxes;
            }
            set { 
                viewRawBoxes = value;
                OnPropertyChanged("ViewRawBoxes");
            }
        }

        Int32 _ChangValueScrollSpeadInmm;

        public RawDataViewModel() 
        {
            _ChangValueScrollSpeadInmm = 1000;
            _start = 0;
            _ScrollSpeadInmm = _ChangValueScrollSpeadInmm;
            ViewLenInmm = 15000;
        }

        double _start;

        public double Start
        {
            get  {
                return _start;
            }
            set
            {
                _start = value;

                foreach (SingleRawDataViewModel RawData in viewRawBoxes)
                {
                    RawData.Start_SRD = value;
                }

                ViewLenInmm = _view_len_in_mm; // ????????????????????????????

                if (viewRawBoxes.Count > 0) _start = viewRawBoxes[0].Start_SRD;

//                OnPropertyChanged("ViewRawBoxes");
                OnPropertyChanged("Start");
            }
        }

        Int32 _ScrollSpeadInmm;
        public Int32 ScrollSpeadInmm
        {
            get
            {
                return _ScrollSpeadInmm;
            }
            set
            {
                _ScrollSpeadInmm = value;
                OnPropertyChanged("ScrollSpeadInmm");
            }
        }


        long _trace_len_in_mm;
        public Int32 TraceLenInmm
        {
            get
            {
                Int32 MaxTL = 0;

                foreach (SingleRawDataViewModel RawData in viewRawBoxes)
                {
                    if (MaxTL < RawData.TraceLenInmm_SRD) MaxTL = RawData.TraceLenInmm_SRD;
                }
                return MaxTL;
            }
            set
            {
                _trace_len_in_mm = value;
                OnPropertyChanged("TraceLenInmm");
            }
        }


        Int32 _view_len_in_mm;
        public Int32 ViewLenInmm
        {
            get
            {
                return _view_len_in_mm;
            }
            set
            {
                _view_len_in_mm = value;

                foreach (SingleRawDataViewModel RawData in viewRawBoxes)
                {
                    RawData.VisibleTraceLen = value;
                }

                OnPropertyChanged("ViewLenInmm");
            }
        }


        private int _index_in_list;

        public int IndexInList
        {

            get { return _index_in_list; }
            set
            {
                _index_in_list = value;
                OnPropertyChanged("IndexInList");

            }
        }

        #region Button commands
        
        public ICommand Command_add_raw {
            get {
                return new CommandImpl(GetTRC_file_and_add_raw);
            }
            set
            {
                new CommandImpl(GetTRC_file_and_add_raw);
            }
        }

        private void GetTRC_file_and_add_raw(object parameter)
        {
            SingleRawDataViewModel tmpRawData = new SingleRawDataViewModel();
            Int32 MaxTL = 0;

            OpenFileDialog openFileDialog = new OpenFileDialog();
            openFileDialog.Filter = "Text files (*.trc)|*.trc|All files (*.*)|*.*";

            if (openFileDialog.ShowDialog() == true)
                tmpRawData.TrcFileName = openFileDialog.FileName;

            tmpRawData.Start_SRD = _start;

            ViewRawBoxes.Add(tmpRawData);

            foreach (SingleRawDataViewModel RawData in viewRawBoxes)
            {
                if (MaxTL < RawData.TraceLenInmm_SRD) MaxTL = RawData.TraceLenInmm_SRD;
            }
            TraceLenInmm = MaxTL;

            OnPropertyChanged("TraceLenInmm");
        }

        public ICommand Command_inc_scroll_spead {
            get {
                return new CommandImpl(inc_scroll_spead);
            }
            set
            {
                new CommandImpl(inc_scroll_spead);
            }
        }

        private void inc_scroll_spead(object parameter)
        {
            ScrollSpeadInmm = _ScrollSpeadInmm + _ChangValueScrollSpeadInmm;
        }

        public ICommand Command_dec_scroll_spead {
            get {
                return new CommandImpl(dec_scroll_spead);
            }
            set
            {
                new CommandImpl(dec_scroll_spead);
            }
        }

        private void dec_scroll_spead(object parameter)
        {
            _ScrollSpeadInmm = _ScrollSpeadInmm - _ChangValueScrollSpeadInmm;
            if (_ScrollSpeadInmm <= 0) _ScrollSpeadInmm = _ChangValueScrollSpeadInmm;

            ScrollSpeadInmm = _ScrollSpeadInmm;
        }

        public ICommand Command_dec_start {
            get {
                return new CommandImpl(dec_start);
            }
            set
            {
                new CommandImpl(dec_start);
            }
        }

        private void dec_start(object parameter)
        {
            Stop_inc_start_timer();
            Start_dec_start_timer();
        }

        public ICommand Command_stop_change_start {
            get {
                return new CommandImpl(stop_change_start);
            }
            set
            {
                new CommandImpl(stop_change_start);
            }
        }

        private void stop_change_start(object parameter)
        {
            Stop_inc_start_timer();
            Stop_dec_start_timer();
        }

        public ICommand Command_inc_start {
            get {
                return new CommandImpl(inc_start);
            }
            set
            {
                new CommandImpl(inc_start);
            }
        }

        private void inc_start(object parameter)
        {
            // Start = start + ScrollSpeadInmm;

            Stop_dec_start_timer();
            Start_inc_start_timer();


            // Определяем анимацию
//            DoubleAnimation ForvardScroll = new DoubleAnimation();
//            ForvardScroll.From = Start;
//            ForvardScroll.To = _trace_len_in_mm;

//            double scroll_time; //в секундах 
//                 scroll_time = (_trace_len_in_mm - Start) / ScrollSpeadInmm;

//                 if (scroll_time <= 0) return;

//            ForvardScroll.Duration = new System.Windows.Duration(TimeSpan.FromSeconds( scroll_time ));


            // Удвоить скорость анимации:
            // a.SpeedRatio = 2;

            // Сделать в 2 раза меньше скорость анимации:
            // a.SpeedRatio = 0.5;

            // Начинаем анимацию
//            my_Slider.BeginAnimation(Slider.Value, ForvardScroll);

//            Inc_start.BeginAnimation(Slider.Value, null); // остановить анимацию

//            Storyboard SlideDownAnim = (Storyboard)((FrameworkElement)container).FindResource("SlideUpAnimation");
//
//            SlideDownAnim.SpeedRatio=0.5; 
//
//            this.BeginStoryboard(SlideDownAnim);

        }

        #endregion //  Button commands

        DispatcherTimer inc_start_timer;  //Объявим поле для таймера

        DispatcherTimer dec_start_timer;  //Объявим поле для таймера


        #region Timer metods

        //Метод для запуска таймера
        private void Start_inc_start_timer()
        {
            if (inc_start_timer == null)
            {
                inc_start_timer = new DispatcherTimer();
                inc_start_timer.Interval = TimeSpan.FromMilliseconds(100);
                inc_start_timer.Tick += inc_start_Tick;
            }

            inc_start_timer.Start();
        }

        private void inc_start_Tick(object sender, EventArgs e)
        {
            if ( _start + ScrollSpeadInmm < _trace_len_in_mm )
                Start = _start + ScrollSpeadInmm;
            else {
                Start = _trace_len_in_mm - _ScrollSpeadInmm;
                Stop_inc_start_timer();
            }
        }

        //Метод остановки таймера
        public void Stop_inc_start_timer()
        {
            if (inc_start_timer != null)
            {
                inc_start_timer.Stop();
                inc_start_timer = null;
            }
        }

        //Метод для запуска таймера
        private void Start_dec_start_timer()
        {
            if (dec_start_timer == null)
            {
                dec_start_timer = new DispatcherTimer();
                dec_start_timer.Interval = TimeSpan.FromMilliseconds(100);
                dec_start_timer.Tick += dec_start_Tick;
            }

            dec_start_timer.Start();
        }

        private void dec_start_Tick(object sender, EventArgs e)
        {
            //Здесь пишем что должен делать таймер
            if (_start - ScrollSpeadInmm >= 0)
                Start = _start - ScrollSpeadInmm;
            else {
                Start = 0;
                Stop_dec_start_timer();
            }
        }

        //Метод остановки таймера
        public void Stop_dec_start_timer()
        {
            if (dec_start_timer != null)
            {
                dec_start_timer.Stop();
                dec_start_timer = null;
            }
        }

        #endregion

        #region Context menu commands

        public ICommand Command_delete_list_index {
            get {
                return new CommandImpl(delete_list_index);
            }
            set
            {
                new CommandImpl(delete_list_index);
            }
        }

        private void delete_list_index(object parameter)
        {
            ViewRawBoxes.RemoveAt(_index_in_list);
            OnPropertyChanged("ViewRawBoxes");
        }

        public ICommand Command_Y_scale {
            get {
                return new CommandImpl(Y_scale);
            }
            set
            {
                new CommandImpl(Y_scale);
            }
        }

        private void Y_scale(object parameter)
        {
            viewRawBoxes[_index_in_list].BrashHeight =  int.Parse((string)parameter);
        }

        #endregion //  Context menu commands

    }
}

