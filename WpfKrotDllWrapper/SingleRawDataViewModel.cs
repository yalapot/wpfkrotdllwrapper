﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.IO;

using Microsoft.Win32; // OpenFileDialog

using krot_dll_wrapper;

namespace WpfKrotDllWrapper.ViewModel
{
    public class SingleRawDataViewModel : ViewModelBase
    {

        NativeMethods myKrotProba;

        public SingleRawDataViewModel()
        {
            TraceLenInmm_SRD = 0;
            start_SRD = 0;
            VisibleTraceLen = 10000;

            trc_file_name = "Не выбран файл";
/*
            // Create source.
            BitmapImage bi = new BitmapImage();
            // BitmapImage.UriSource must be in a BeginInit/EndInit block.
            bi.BeginInit();
            bi.UriSource = new Uri("..\\..\\Resources\\gpas_logo.jpg ",UriKind.RelativeOrAbsolute);
            bi.EndInit();
            // Set the image source.
            RawDataBrash = new ImageBrush(bi);
*/
        }

        OdoTrlConverter Odo_Trl_Converter;

        public void Init(string path_to_trc)
        {
            if (myKrotProba != null) myKrotProba.myCloseTrace();

            Int32 TrcHandle = (Int32)DateTime.Now.Ticks;
            if (TrcHandle < 0) TrcHandle = -TrcHandle;

            trc_file_name = path_to_trc;
            myKrotProba = new NativeMethods(path_to_trc, TrcHandle);
            VisibleTraceLen = 10000;
            BrashHeight = 100;

            TraceLenInmm_SRD = myKrotProba.myOpenTrace();

            Odo_Trl_Converter = new OdoTrlConverter(System.IO.Path.GetDirectoryName(trc_file_name) );

            pred_start = -1;
            Start_SRD = 0;
        }


        string trc_file_name;

        public string TrcFileName
        {
            get
            {
                return trc_file_name;
            }
            set
            {
                trc_file_name = value;
                Init(trc_file_name);

                OnPropertyChanged("TrcFileName");
            }
        }


        Int32 start_SRD;
        Int32 pred_start;

        public double Start_SRD
        {
            get {
                return start_SRD;
            }
            set
              {
                  start_SRD = (Int32) value;

                  if (myKrotProba != null &&  pred_start != start_SRD )
                  {


                      pred_start = start_SRD;

                      Int32 request_start = (Int32) Odo_Trl_Converter.ReportOdom2TraceOdom((double)start_SRD);
                      Int32 request_end = (Int32) Odo_Trl_Converter.ReportOdom2TraceOdom((double)(start_SRD + VisibleTraceLen));
                      Int32 request_trace_len = request_end - request_start;

                      RawData = myKrotProba.myPaint(request_start, request_trace_len);
                      OnPropertyChanged("Start_SRD");
                  }
              }
        }


        ImageBrush RawDataBrash;

        public ImageBrush RawData
        {
            get {
                return RawDataBrash;
            }
            set
            {
                RawDataBrash = value;
                OnPropertyChanged("RawData");
            }
        }


        Int32 trace_len_in_mm;

        public Int32 TraceLenInmm_SRD
        {
            get
            {
                return trace_len_in_mm;
            }
            set
            {
                trace_len_in_mm = value;
                OnPropertyChanged("TraceLenInmm_SRD");
            }
        }


        Int32 visible_trace_len;

        public Int32 VisibleTraceLen
        {
            get
            {
                return visible_trace_len;
            }
            set
            {
                visible_trace_len = value;
                OnPropertyChanged("VisibleTraceLen");
            }
        }

        int _brash_height;

        public int BrashHeight
        {
            get
            {
                return _brash_height;
            }
            set
            {
                _brash_height = value;
                OnPropertyChanged("BrashHeight");
            }
        }

        protected override void OnDispose()
        {
            if (myKrotProba != null) myKrotProba.myCloseTrace();
        }


        #region Button command
        
        public ICommand CommandChooseTRC {
            get {
                return new CommandImpl(GetTRC_file);
            }
            set
            {
                new CommandImpl(GetTRC_file);
            }
        }

        public void GetTRC_file(object parameter)
        {
            OpenFileDialog openFileDialog = new OpenFileDialog();
            openFileDialog.Filter = "Text files (*.trc)|*.trc|All files (*.*)|*.*";

            if (openFileDialog.ShowDialog() == true)
                TrcFileName = openFileDialog.FileName;
        }

        #endregion //  Button command

    }
}
