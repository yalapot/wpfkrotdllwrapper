﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel;
using System.Data.SQLite;
using System.Data.Common;

namespace WpfKrotDllWrapper
{
    class OdoTrlRecord
    {
        public double odo_s;
        public double odo_e;
        public double rpt_s;
        public double rpt_e;
    }

    public class OdoTrlConverter
    {
        DbConnection DataBaseConnection;

        double StartShift;
        double odo_coef;

        List<OdoTrlRecord> tbl_koef;

        protected OdoTrlConverter()
        {
        }

        public OdoTrlConverter(string OdoTrlFolderPath)
        {
            string SQLiteConnectionString = "Data Source=" +
                                             OdoTrlFolderPath + "\\odo.trl;Version=3;";

            DataBaseConnection = new SQLiteConnection(SQLiteConnectionString);
            DataBaseConnection.Open();

            var LoadedModel = new SQLiteCommand("SELECT odo_start FROM cfg", (SQLiteConnection)DataBaseConnection);
            try
            {
                StartShift = Convert.ToDouble(LoadedModel.ExecuteScalar());
            }
            catch (System.Data.SQLite.SQLiteException)
            {
                StartShift = 0;
            }

            LoadedModel = new SQLiteCommand("SELECT odo_koef FROM cfg", (SQLiteConnection)DataBaseConnection);
            try
            {
                odo_coef = Convert.ToDouble(LoadedModel.ExecuteScalar());
            }
            catch (System.Data.SQLite.SQLiteException)
            {
                odo_coef = 1.0;
            }

            SQLiteCommand command = new SQLiteCommand("SELECT * FROM tbl_koef", (SQLiteConnection)DataBaseConnection);
            try
            {
                SQLiteDataReader reader = command.ExecuteReader();

                tbl_koef = new List<OdoTrlRecord>();

                foreach (DbDataRecord record in reader)
                {
                     OdoTrlRecord tmpRecord = new OdoTrlRecord();

                     tmpRecord.odo_s = Convert.ToDouble(record["odo_s"]);
                     tmpRecord.odo_e = Convert.ToDouble(record["odo_e"]);
                     tmpRecord.rpt_s = Convert.ToDouble(record["rpt_s"]);
                     tmpRecord.rpt_e = Convert.ToDouble(record["rpt_e"]);

                     tbl_koef.Add(tmpRecord);
                }

                tbl_koef.Sort(delegate (OdoTrlRecord otr1, OdoTrlRecord otr2) { return otr1.odo_s.CompareTo(otr2.odo_s); });
            }
            catch (System.Data.SQLite.SQLiteException)
            {
                tbl_koef = null;
            }

            DataBaseConnection.Close();
            System.Data.SQLite.SQLiteConnection.ClearAllPools();

        } //     public OdoTrlConverter(string OdoTrlFolderPath)

        public double ReportOdom2TraceOdom (double ReportOdom)
        {
            //            var selectedDiapazon = from Diapazon in tbl_koef
            //                                            where Diapazon.rpt_s <= ReportOdom
            //                                            where Diapazon.rpt_e >= ReportOdom
            //                                            select Diapazon;
            //
            //            if  (selectedDiapazon.count > 0)
            //            {
            //                double tmp_odom_koef;
            //
            //                selectedDiapazon[0].rpt_s
            //            }

            ReportOdom = ReportOdom + StartShift;

            if (tbl_koef != null)
            {
                foreach (OdoTrlRecord record in tbl_koef)
                {
                    if ( ReportOdom >= record.rpt_s && ReportOdom < record.rpt_e)
                    {
                        double tmp_odom_koef = (record.rpt_e - record.rpt_s) / (record.odo_e - record.odo_s);

                        return record.odo_s + ( ReportOdom - record.rpt_s) / tmp_odom_koef;
                    }
                }
            }

            return ReportOdom / odo_coef;
        }

        public double TraceOdom2ReportOdom (double TraceOdom)
        {
            return TraceOdom;
        }
    }
}
             