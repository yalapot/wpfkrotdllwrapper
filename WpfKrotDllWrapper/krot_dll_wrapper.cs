﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
//using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
//using System.Windows.Shapes;
using System.Runtime.InteropServices;
using System.IO;


namespace krot_dll_wrapper
{
    /// <summary>
    /// Create a New INI file to store or load data
    /// </summary>
    public class IniFile
    {
        public string path;

        [DllImport("kernel32")]
        private static extern long WritePrivateProfileString(string section,
            string key, string val, string filePath);
        [DllImport("kernel32")]
        private static extern int GetPrivateProfileString(string section,
                 string key, string def, StringBuilder retVal,
            int size, string filePath);

        /// <summary>
        /// INIFile Constructor.
        /// </summary>
        /// <PARAM name="INIPath"></PARAM>
        public IniFile(string INIPath)
        {
            path = INIPath;
        }
        /// <summary>
        /// Write Data to the INI File
        /// </summary>
        /// <PARAM name="Section"></PARAM>
        /// Section name
        /// <PARAM name="Key"></PARAM>
        /// Key Name
        /// <PARAM name="Value"></PARAM>
        /// Value Name
        public void IniWriteValue(string Section, string Key, string Value)
        {
            WritePrivateProfileString(Section, Key, Value, this.path);
        }

        /// <summary>
        /// Read Data Value From the Ini File
        /// </summary>
        /// <PARAM name="Section"></PARAM>
        /// <PARAM name="Key"></PARAM>
        /// <PARAM name="Path"></PARAM>
        /// <returns></returns>
        public string IniReadValue(string Section, string Key)
        {
            StringBuilder temp = new StringBuilder(255);
            int i = GetPrivateProfileString(Section, Key, "", temp,
                                            255, this.path);
            return temp.ToString();

        }
    }


    // структура описания произвольного датчика
    // Declares a managed structure for each unmanaged structure.
    [StructLayout(LayoutKind.Sequential, CharSet = CharSet.Ansi)]
    public struct T_SENS
    {
        public Int32 num;    // количество
        public Int32 step;   // дискретность измерения (мм/милисек - от метода опроса)
        public Int32 min;    // минимальное значение (размерность аналогично T_NODEGROUP)
        public Int32 max;    // максимальное значение (размерность аналогично T_NODEGROUP)
    };

    // структура для передачи в VB информации об открытом прогоне
    [StructLayout(LayoutKind.Sequential)]
    public struct VB_TRACE_INFO
    {
        public Int32 onLine;          // флаг функционирования динамического обновления данных
        public Int32 crzZoneNum;      // количество поясов короз.датчиков
        public Int32 extSensors;      // кол-во вспомогательных датчиков
        public Int32 VOG;             // функция VOG
        public Int32 evnt;            // поддержка событий

        public T_SENS odom;          // одометры (разница .max - .min это длина трассы)
        public T_SENS timer;         // таймеры (разница .max - .min это длительность прогона)

        public T_SENS shake;         // ускорение
        public T_SENS press;         // давление
        public T_SENS temp;          // температура
        public T_SENS angle;         // угловые скорости
        public T_SENS wall;          // толщина стенки трубы

        public T_SENS orient;        // ориентация
        public T_SENS speed;         // скорость в см/сек
    };

    // структура описания пояса датчиков развертки
    [StructLayout(LayoutKind.Sequential)]
    public struct T_SENSGROUP
    {
        public Int32 num;       // кол-во датчиков в поясе
        public Int32 type;      // тип датчиков (SENS_TYPE_*)
        public Int32 minValue;  // минимальное значение показаний датчика (единицы АЦП)
        public Int32 maxValue;  // максимальное значение показаний датчика (единицы АЦП)
        public Int32 isRow;     // 1/0 если 0 сырые данные не поддерживаются
        public Int32 maxRowVal; // максимальное значение сырых данных 
        public Int32 minRowVal; // максимальное значение сырых данных 
    };

    // структура описания параметров окна отображения развертки в режиме цветовой карты
    [StructLayout(LayoutKind.Sequential)]
    public struct VB_PAINT_INFO
    {
        public Int32 orntOff;         // флаг отключения ориентации
        public Int32 baseLine;        // положение базовой линии для выравнивания
        public Int32 amplif;          // коэф-нт усиления
        public Int32 intwin;          // длина окна интегрирования
    };

    // структура описания параметров окна отображения развертки в режиме графиков
    [StructLayout(LayoutKind.Sequential)]
    public struct VB_GRAPH_INFO
    {
        public Int32 amplif;          // коэф-нт усиления
        public Int32 gap;             // величина прореживания
        public Int32 clrBackGround;   // цвет фона
        public Int32 clrOdd;          // цвет нечетных графиков
        public Int32 clrEven;         // цвет четных графиков
    };

    /* Структура элемента палитры */
    [StructLayout(LayoutKind.Sequential)]
    public struct VB_PAL_ITEM
    {
        public Int32 iPos;                  // положение на векторе элементов
        public Int32 iVal;                     // значение полного кода цвета

        public VB_PAL_ITEM(Int32 pos, Int32 val)
        {
            iPos = pos;
            iVal = val;
        }
    };

    /* Структура палитры пользователя */
    [StructLayout(LayoutKind.Sequential)]
    public struct VB_PAL
    {
        public Int32 itemNum;           // кол-во элементов в массиве item[]
        public VB_PAL_ITEM minItem;    // первый элемент палитры
        public VB_PAL_ITEM maxItem;    // последний элемент палитры
    };

    internal class NativeMethods
    {
        [DllImport("KrotW32.dll", CharSet = CharSet.Ansi)]
        public static extern Int16 CS_krotPaint(
                                              Int32 trcHandle,
                                              Int32 PoyasIdx,
                                              Int32 start,

                                              Int32 x_size,
                                              Int32 y_size,
                                              byte[] bmp_buf
                                             );

        [DllImport("KrotW32.dll", CharSet = CharSet.Ansi)]
        private static extern Int16 krotOpenTrace(
                                                   Int32 trcHandle,
                                                   StringBuilder fileName,
                                                   StringBuilder indxFolder,
                                                   StringBuilder driverFileName,
                                                   Int32 OnLineAddData,
                                                   ref VB_TRACE_INFO inf
                                                  );

        [DllImport("KrotW32.dll", CharSet = CharSet.Ansi)]
        private static extern Int16 krotCloseTrace(Int32 trcHandle);

        [DllImport("KrotW32.dll", CharSet = CharSet.Ansi)]
        private static extern Int16 krotExtCorozInfo/*_fixInf*/(
                                                      Int32 trcHandle,
                                                      Int32 crzIndx,
                                                      ref T_SENSGROUP cinf
                                                    );

        [DllImport("KrotW32.dll", CharSet = CharSet.Ansi)]
        private static extern Int16 krotScreenMode(
                                                     Int32 trcHandle,
                                                     Int32 crzIndx,
                                                     byte[] sens,
                                                     ref VB_PAINT_INFO vbScreen,
                                                     ref VB_GRAPH_INFO vbGraphs
                                                   );

        [DllImport("KrotW32.dll", CharSet = CharSet.Ansi)]
        private static extern Int16 CS_krotChangePic(
                                                   Int32 trcHandle,
                                                   Int32 PoyasIdx,
                                                   Int32 x_size,
                                                   Int32 y_size
                                                  );

        /*
         Функция устанавливает данные палитры для развертки crzIndx прогона Handle.
         Если Handle == -1, то данные устанавливаются для редактора палитр.
         аргумент item это указатель на массив элементов палитры между первым и последним.
         должны быть отсортированы по item->iPos по возрастанию.
         Возвращает KRT_OK при успехе, KRT_ERR при ошибке.
        */
        [DllImport("KrotW32.dll", CharSet = CharSet.Ansi)]
        private static extern Int16 krtPalSet(
                                                   Int32 trcHandle,
                                                   Int32 crzIndx,
                                                   Int32 brdMin,
                                                   Int32 brdMax,
                                                   Int32 usrMin,
                                                   Int32 usrMax,
                                                   ref VB_PAL pal,
                                                   ref VB_PAL_ITEM item
                                                  );

        [DllImport("KrotW32.dll", CharSet = CharSet.Ansi)]
        private static extern Int16 checkVmode();


        Int32 myTraceHandle;


        StringBuilder myFileName;
        StringBuilder myIdxFolder;
        StringBuilder myDriverFileName;

        VB_TRACE_INFO myTraceInfo;

        T_SENSGROUP mySensGroup;
        Int32 mySensNum;
        VB_PAINT_INFO myPaintInfo;
        VB_GRAPH_INFO myGraphInfo;
        byte[] mySens;
        VB_PAL_ITEM[] myPalItems;
        VB_PAL myPal;


        Int32 odom_step = 1;
        Int32 trace_len_in_mm = 0;


        protected NativeMethods()
        {
            myTraceHandle = 1;
            myFileName = new StringBuilder("..\\..\\700_intr_stend_190703\\stend_190703.trc");
            myIdxFolder = new StringBuilder("");
            myDriverFileName = new StringBuilder("..\\..\\700_intr_stend_190703\\coros_700_8201_MFL_plus_v2.dll");
        }


        public NativeMethods(string path_to_trc, Int32 trc_handle)
        {
            myTraceHandle = trc_handle;
            myIdxFolder = new StringBuilder("");

            myFileName = new StringBuilder(path_to_trc);

            string path = System.IO.Path.GetDirectoryName(path_to_trc);

            IniFile ini = new IniFile(path_to_trc);

            string drv_path_key = ini.IniReadValue("Driver", "Path");


            string dll_name = System.IO.Path.GetFileName(drv_path_key);

            myDriverFileName = new StringBuilder(path + "\\" + dll_name);
        }


        internal int myOpenTrace()
        {
            Int16 res = 10;
            myTraceInfo = new VB_TRACE_INFO();
            mySensGroup = new T_SENSGROUP();

            res = checkVmode();

            res = krotOpenTrace(myTraceHandle,
                                 myFileName,
                                 myIdxFolder,
                                 myDriverFileName,
                                 0,
                                 ref myTraceInfo
                                );

            odom_step = myTraceInfo.odom.step;
            trace_len_in_mm = myTraceInfo.odom.max - myTraceInfo.odom.min;

            res = krotExtCorozInfo(
                                    myTraceHandle,
                                     0,
                                     ref mySensGroup
                                   );

            mySensNum = mySensGroup.num;

            myPal = new VB_PAL();
            myPal.itemNum = 7;
            myPal.minItem = new VB_PAL_ITEM(0, 0);
            myPal.maxItem = new VB_PAL_ITEM(199, 13823);

            myPalItems = new VB_PAL_ITEM[7];

            myPalItems[0] = new VB_PAL_ITEM(78, 11579568);
            myPalItems[1] = new VB_PAL_ITEM(89, 12632256);
            myPalItems[2] = new VB_PAL_ITEM(110, 15863040);
            myPalItems[3] = new VB_PAL_ITEM(128, 587520);
            myPalItems[4] = new VB_PAL_ITEM(137, 65384);
            myPalItems[5] = new VB_PAL_ITEM(151, 62975);
            myPalItems[6] = new VB_PAL_ITEM(154, 59135);

            res = krtPalSet(
                             myTraceHandle,
                             0,
                             0,   // Int32 brdMin,
                             199, // Int32 brdMax,
                             0,   // Int32 usrMin,
                             199, // Int32 usrMax,
                             ref myPal,
                             ref myPalItems[0]
                            );

            myPaintInfo = new VB_PAINT_INFO();
            myPaintInfo.orntOff = 0;
            myPaintInfo.baseLine = 0;
            myPaintInfo.amplif = 0;

            myGraphInfo = new VB_GRAPH_INFO();
            myGraphInfo.amplif = 5;
            myGraphInfo.gap = 3;
            myGraphInfo.clrBackGround = 0xF0F0F0;
            myGraphInfo.clrOdd = 0x000000;
            myGraphInfo.clrEven = 0x0000FF;

            mySens = new byte[mySensNum * 256];

            res = krotScreenMode(myTraceHandle,
                                  0,
                                  mySens,
                                  ref myPaintInfo,
                                  ref myGraphInfo
                                );

            return trace_len_in_mm;
        } // myOpenTrace()

        byte[] array;
        MemoryStream memorystream;
        BitmapImage imgsource;
        ImageBrush ret_IB;

        internal ImageBrush myPaint(Int32 trc_start, Int32 trc_length)
        {
            int res = 10;

            const int byte_per_pixel = 4;
            const int BMP_header_size = 54;

            int win_length_in_pixel = trc_length/odom_step;

            //            GC.Collect();

            array = new byte[win_length_in_pixel * mySensNum * byte_per_pixel + BMP_header_size];

            res = CS_krotChangePic(myTraceHandle, 0, win_length_in_pixel, mySensNum);

            res = CS_krotPaint(myTraceHandle, 0, trc_start, win_length_in_pixel, mySensNum, array);

            memorystream = new MemoryStream();
            memorystream.Write(array, 0, (int)array.Length);

            imgsource = new BitmapImage();
            imgsource.BeginInit();
            imgsource.StreamSource = memorystream;
            imgsource.EndInit();

            ret_IB = new ImageBrush(imgsource);

            return ret_IB;
        }

        internal int myCloseTrace()
        {
            krotCloseTrace(myTraceHandle);
            return 0;
        }
    };
}
