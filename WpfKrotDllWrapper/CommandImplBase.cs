﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel;
using System.Windows.Input;

namespace WpfKrotDllWrapper
{
    class CommandImpl : ICommand
    {
        private readonly Action<object> _action = null;
        public CommandImpl(Action<object> action)
        {
            _action = action;
        }

        public event EventHandler CanExecuteChanged;
        public bool CanExecute(object parameter) { return true; }
        public void Execute(object parameter) { _action(parameter); }
    }
}
